"""Application instance initialization."""

from pathlib import Path

from kaiju_tools import App
from kaiju_tools.app import init_app as init_app_base

from app.tables import METADATA
from app.urls import URLS

__all__ = ["init_app"]


def init_app(settings, **kws) -> App:
    app = init_app_base(settings, attrs={"db_meta": METADATA}, **kws)
    for method, reg, handler, name in URLS:
        app.router.add_route(method, reg, handler)
        app.router.add_route(method, reg + "/", handler, name=name)
    return app
