import asyncio

from kaiju_tools.app import Scheduler
from kaiju_tools.interfaces import Locks
from kaiju_redis import RedisListener
from kaiju_tools.functions import retry
from .history import HistoryService


class HistoryConsumer(RedisListener):
    _history: HistoryService
    lock_check_interval = 5
    async def init(self):
        self._closing = False
        self._unlocked.set()
        self._idle.set()
        self._locks = self.discover_service(self._locks, cls=Locks)
        self._transport = self.get_transport()
        self._scheduler = self.discover_service(self._scheduler, cls=Scheduler)
        self._loop = asyncio.create_task(self._read(), name=f"{self.service_name}.{self.topic}._read")
        self._lock_task = self._scheduler.schedule_task(
            self._check_lock, interval=self.lock_check_interval, name=f"{self.service_name}._check_lock"
        )
        await self._create_group()
        if self.pending_messages_time_ms:
            interval = max(1, self.pending_messages_time_ms // 1000)
            self._task_claim_pending = self._scheduler.schedule_task(
                self._claim_pending, interval=interval, name=f"{self.service_name}._claim_pending"
            )
        if self.trim_size:
            self._task_trim_records = self._scheduler.schedule_task(
                self._trim_records, interval=self._trim_interval, name=f"{self.service_name}._trim_records"
            )
        self._history = self.discover_service(None, cls=HistoryService)

    async def _process_batch(self, batch):
        acks = []
        list_parameters = []
        for row in batch:
            row_id, data = row
            data = self._loads(data[b"_"])
            list_parameters.append(data[0])
            acks.append(row_id)
        if list_parameters:
            self.logger.debug("Stream data: %s", list_parameters)
            await self._history.import_history(list_parameters)
            self.logger.info("Stream %s batch count: %s", self.service_name, len(list_parameters))
        if acks:
            await retry(
                self._transport.xack,
                args=(self._key, self.group_id),
                kws={"identifiers": acks},
                retries=5,
                retry_timeout=1.0,
                max_retry_timeout=10,
                logger=self.logger,
            )
