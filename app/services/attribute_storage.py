import uuid
import sqlalchemy.dialects.postgresql as pg_sa

from kaiju_db import DatabaseService
from kaiju_tools.encoding import loads
from kaiju_tools.services import ContextableService

from .. import tables
from .utils import generate_hash


class AttributeStorageService(ContextableService):
    service_name = "AStorage"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def init(self):
        await super().init()
        self._db = self.discover_service(None, cls=DatabaseService)

    async def get_dict_by_name(self, list_name):
        attribute_dict_by_name = {}
        list_for_db = []
        list_name = list(set(list_name))
        list_hash = []
        for i in range(len(list_name)):
            name, meta = list_name[i]
            if type(name) is not str:
                name = str(name)
            list_hash.append(uuid.UUID(generate_hash(name, meta)))
        select_statement = tables.attribute.select().where(tables.attribute.c.id.in_(list_hash))
        db_response_select = await self._db.fetch(select_statement)
        for item in db_response_select:
            attribute = dict(item)
            attribute_dict_by_name[(attribute["attribute_name"], attribute["meta"])] = attribute["id"]
        set_names = set(list_name)
        set_names = set_names.difference(set(attribute_dict_by_name.keys()))
        for attribute_name, meta in set_names:
            list_for_db.append(
                {
                    "id": uuid.UUID(generate_hash(attribute_name, meta)),
                    "attribute_name": attribute_name,
                    "meta": str(meta),
                }
            )
        if set_names:
            stm = (
                pg_sa.insert(tables.attribute).values(list_for_db).on_conflict_do_nothing().returning(tables.attribute)
            )
            db_response = await self._db.fetch(stm)
            for attribute in db_response:
                attribute_dict_by_name[(attribute["attribute_name"], attribute["meta"])] = attribute["id"]
        self.logger.debug(attribute_dict_by_name)
        return attribute_dict_by_name

    async def get_dict_by_id(self, list_id):
        statement = tables.attribute.select().where(tables.attribute.c.id.in_(list_id))
        attribute_temp = await self._db.fetch(statement)
        attribute_dict_by_id = {}
        for i in attribute_temp:
            attribute = dict(i)
            self.logger.debug(attribute)
            attribute_dict_by_id[attribute["id"]] = {
                "id": attribute["attribute_name"],
                "projection": loads(attribute["meta"]),
            }
        return attribute_dict_by_id
