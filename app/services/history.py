import uuid

from sqlalchemy import and_, desc, text, tuple_, func, bindparam
import sqlalchemy.dialects.postgresql as pg_sa
import sqlalchemy as sa

from kaiju_tools import ContextableService
from kaiju_tools import AbstractRPCCompatible
from kaiju_tools.encoding import dumps
from kaiju_db import DatabaseService
from kaiju_tools.jsonschema import Object, AnyOf, String, GUID, Integer, Generic, Array, Null


import app.tables as tables
from .utils import to_string_list, generate_hash


class HistoryService(ContextableService, AbstractRPCCompatible):
    service_name = "HistoryService"
    _db: DatabaseService

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def init(self):
        await super().init()
        self._db = self.discover_service(None, cls=DatabaseService)

    @property
    def routes(self):
        return {
            "import_one": self.app.services.HistoryProducer.import_item,
            "import": self.app.services.HistoryProducer.import_items,
            "list_history": self.list_history,
            "clear": self.clear_history,
        }

    @property
    def validators(self) -> dict:
        metadata = Object(
            {
                "author": AnyOf(String(), GUID()),
                "tags": Object(nullable=True),
                "ttl": Integer(minimum=1),
            },
            additionalProperties=True,
            required=["ttl"],
        )
        correlation_id = AnyOf(String(), GUID())
        version = Integer(minimum=0)
        attribute = Object(
            {"version": version, "data": Generic(), "name": String(), "meta": Generic()},
            required=["data", "name"],
        )
        attributes = Array(attribute, minItems=1)
        offset = Integer(minimum=0)
        limit = Integer(minimum=1)
        authors = AnyOf(Array(AnyOf(String(), GUID()), minItems=1), Null())
        tags = AnyOf(Array(String(), minItems=1), Null())
        return {
            "import": Object(
                {
                    "list_items": AnyOf(
                        Array(
                            Object(
                                {
                                    "correlation_id": correlation_id,
                                    "attributes": attributes,
                                    "version": version,
                                    "metadata": metadata,
                                },
                                required=["correlation_id", "attributes", "version", "metadata"],
                            ),
                            minItems=0,
                        ),
                        Null(),
                    )
                },
                additionalProperties=False,
                required=["list_items"],
            ),
            "list_history": Object(
                {"correlation_id": correlation_id, "limit": limit, "offset": offset, "author": authors, "tags": tags},
                additionalProperties=True,
                required=["correlation_id"],
            ),
        }

    @property
    def permissions(self):
        return {self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION}

    async def import_history(self, list_history):
        """
        :param list_history: [
        {
            correlation_id: text,
            version: integer
            metadata: {
                author: text,
                tag: {},
                ttl: int(days)
            }
            attributes:[
                {
                version: integer,
                name: text
                data: {}
                }
            ]
        }

        ]
        :return:
        """
        list_attribute = []
        for item in list_history:
            for attribute in item["attributes"]:
                meta = dumps(attribute.get("meta", [])).decode("utf-8")
                list_attribute.append((attribute["name"], meta))
        attribute_dict = await self.app.services.AStorage.get_dict_by_name(list_attribute)
        data_for_history_version = []
        data_for_history = []
        data_for_data = []
        for item in list_history:
            if type(item["correlation_id"]) is not str:
                item["correlation_id"] = str(item["correlation_id"])
            item["correlation_id"] = uuid.UUID(generate_hash(item["correlation_id"]))
            author = item["metadata"].pop("author", None)

            if type(author) is not str:
                author = str(author)
            user_tag = item["metadata"].pop("tag", None)
            if user_tag is None:
                user_tag = []
            for attribute in item["attributes"]:
                data_hash = generate_hash(dumps(attribute.get("data", {})).decode("utf-8"))
                user_tag.append(attribute["name"])
                meta_str = dumps(attribute.get("meta", [])).decode("utf-8")
                data_for_history.append(
                    {
                        "correlation_id": item["correlation_id"],
                        "version": item["version"],
                        "attribute": attribute_dict[(attribute["name"], meta_str)],
                        "attribute_version": attribute.get("version", 0),
                        "data_hash": data_hash,
                    }
                )
                data_for_data.append({"data_hash": data_hash, "data": attribute.get("data", {})})
            data_for_history_version.append(
                {
                    "correlation_id": item["correlation_id"],
                    "version": item["version"],
                    "author": author,
                    "tag": user_tag,
                    "ttl": item["metadata"].pop("ttl", None),
                    "meta": item["metadata"],
                }
            )
        smt_insert_h = pg_sa.insert(tables.history).values(data_for_history).on_conflict_do_nothing()
        smt_insert_h_version = (
            pg_sa.insert(tables.history_version).values(data_for_history_version).on_conflict_do_nothing()
        )
        stm_insert_d = pg_sa.insert(tables.data).values(data_for_data).on_conflict_do_nothing()
        await self._db.execute(smt_insert_h)
        await self._db.execute(smt_insert_h_version)
        await self._db.execute(stm_insert_d)
        return True

    async def clear_history(self):
        statement_delete = (
            tables.history_version.delete()
            .where(text("created::DATE + ttl <= now()::DATE"))
            .returning(tables.history_version.c.correlation_id, tables.history_version.c.version)
        )
        tuple_for_delete = tuple_(tables.history.c.correlation_id, tables.history.c.version)
        list_for_delete_history = []
        delete_response = await self._db.fetch(statement_delete)
        for i in delete_response:
            item = dict(i)
            list_for_delete_history.append((item["correlation_id"], item["version"]))
        statement_delete_history = (
            tables.history.delete()
            .where(tuple_for_delete.in_(list_for_delete_history))
            .returning(tables.history.c.data_hash, tables.history.c.attribute)
        )
        delete_response_history = await self._db.fetch(statement_delete_history)
        set_data_hash = set()
        set_attribute = set()
        for i in delete_response_history:
            item = dict(i)
            set_data_hash.add(item["data_hash"])
            set_attribute.add(item["attribute"])
        if set_data_hash:
            inner_select = (
                tables.history.select()
                .with_only_columns([tables.history.c.data_hash])
                .where(tables.history.c.data_hash.in_(set_data_hash))
            )
            delete_data = tables.data.delete().where(
                and_(tables.data.c.data_hash.notin_(inner_select), tables.data.c.data_hash.in_(set_data_hash))
            )
            await self._db.execute(delete_data)
        if set_attribute:
            inner_select = (
                tables.history.select()
                .with_only_columns([tables.history.c.attribute])
                .where(tables.history.c.attribute.in_(set_attribute))
            )
            delete_attribute = tables.attribute.delete().where(
                and_(tables.attribute.c.id.notin_(inner_select), tables.attribute.c.id.in_(set_attribute))
            )
            await self._db.execute(delete_attribute)

    async def _get_dict_attribute(self, correlation_id, versions, old_value):
        response_dict = {}
        attribute_id_list = []
        data_hash_list = []

        for version in versions:
            response_dict[version] = {"attributes": {}}

        attributes = None
        if old_value:
            attributes = await self._db.fetch(
                f"""
                    SELECT history.version, history.attribute, history.data_hash, 
                    old.data_hash as old_data_hash,  old.attribute_version as old_attribute_version from history
                    left join history as old ON (
                        history.correlation_id = old.correlation_id and 
                        history.attribute = old.attribute and
                        old.attribute_version = history.attribute_version - 1
                        )
                        WHERE history.correlation_id =  {bindparam('correlation_id_')}
                        AND history.version = any({bindparam('versions_')} ::integer[])
                """,
                {"correlation_id_": str(correlation_id), "versions_": versions},
            )
        else:
            attributes = await self._db.fetch(
                tables.history.select().where(
                    and_(tables.history.c.correlation_id == correlation_id, tables.history.c.version.in_(versions))
                )
            )
        for item in attributes:
            item = dict(item)
            attribute_id_list.append(item["attribute"])
            response_dict[item["version"]]["attributes"][item["attribute"]] = {
                "attribute": item["attribute"],
                "current": item["data_hash"],
            }
            attribute_id_list.append(item["attribute"])
            data_hash_list.append(item["data_hash"])
            if old_value:
                if item["old_attribute_version"] is not None:
                    response_dict[item["version"]]["attributes"][item["attribute"]]["old"] = item["old_data_hash"]
                    data_hash_list.append(item["old_data_hash"])
                else:
                    response_dict[item["version"]]["attributes"][item["attribute"]]["old"] = "Значение не известно"
        attributes_dict = await self.app.services.AStorage.get_dict_by_id(attribute_id_list)
        statement_select_data = tables.data.select().where(tables.data.c.data_hash.in_(data_hash_list))
        data_result = await self._db.fetch(statement_select_data)
        dict_data = {}
        for i in data_result:
            data = dict(i)
            dict_data[data["data_hash"]] = data["data"]

        for version in response_dict.keys():
            for attribute in response_dict[version]["attributes"].keys():
                response_dict[version]["attributes"][attribute]["attribute"] = attributes_dict[attribute]
                data_hash_current = response_dict[version]["attributes"][attribute]["current"]
                response_dict[version]["attributes"][attribute]["current"] = dict_data.get(
                    data_hash_current, data_hash_current
                )
                if old_value:
                    data_hash_old = response_dict[version]["attributes"][attribute]["old"]
                    response_dict[version]["attributes"][attribute]["old"] = dict_data.get(data_hash_old, data_hash_old)
            response_dict[version]["attributes"] = list(response_dict[version]["attributes"].values())

        return response_dict

    async def _list_history(self, conditions, offset, limit, correlation_id, old_value, raw_c_id):
        statement_meta = (
            tables.history_version.select()
            .where(sa.and_(*conditions))
            .order_by(desc(tables.history_version.c.version))
            .offset(offset)
            .limit(limit)
        )
        count_statement = sa.select(func.count()).select_from(tables.history_version).where(sa.and_(*conditions))
        meta_data = await self._db.fetch(statement_meta)
        count = await self._db.fetchval(count_statement)
        versions = []
        response_dict = {}
        for item in meta_data:
            item = dict(item)
            response_dict[item["version"]] = {
                "correlation_id": raw_c_id,
                "author": item["author"],
                "tag": item["tag"],
                "meta": item["meta"],
                "version": item["version"],
                "created": item["created"],
            }
            versions.append(item["version"])
        attributes = await self._get_dict_attribute(correlation_id, versions, old_value)
        for key, data in response_dict.items():
            response_dict[key]["attributes"] = attributes[key]["attributes"]
        response = list(response_dict.values())

        return {"data": response, "count": count}

    async def list_history(self, correlation_id, offset=0, limit=10, author=None, tag=None, old_value=True):
        if type(correlation_id) is not str:
            correlation_id = str(correlation_id)
        r_c_id = correlation_id
        correlation_id = uuid.UUID(generate_hash(correlation_id))
        conditions = [tables.history_version.c.correlation_id == correlation_id]
        if author is not None:
            author = to_string_list(author)
            conditions.append(tables.history_version.c.author.in_(author))
        if tag is not None:
            tag = to_string_list(tag)
            conditions.append(tables.history_version.c.tag.contains(tag))
        return await self._list_history(conditions, offset, limit, correlation_id, old_value, r_c_id)
