import logging
from kaiju_redis import RedisStreamRPCClient, RedisTransportService


class HistoryProducer(RedisStreamRPCClient):
    service_name = "HistoryProducer"

    def __init__(self, *args, topic_name="history", **kwargs):
        super().__init__(*args, **kwargs)
        self.topic_name = topic_name
        self._topic = (self.app.namespace/'_stream').get_key(topic_name)
        self.logger.setLevel(logging.WARNING)

    async def _init(self):
        self._transport: RedisTransportService = self.discover_service(self._transport, cls=RedisTransportService)

    async def push(self, body):
        return await self.write(topic=self._topic, body=body)

    async def import_item(self, correlation_id, attributes, version, metadata):
        await self.write(
            topic=self._topic,
            body={
                "correlation_id": correlation_id,
                "attributes": attributes,
                "version": version,
                "metadata": metadata,
            },
        )

    async def import_items(self, list_items):
        self.logger.info(list_items)
        if list_items is None:
            list_items = []
        for i in list_items:
            await self.write(topic=self._topic, body=i)
