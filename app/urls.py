from kaiju_tools.http import JSONRPCView


URLS = [
    ('*', '/public/rpc', JSONRPCView, 'json_rpc_view'),
]  #: web app routes
