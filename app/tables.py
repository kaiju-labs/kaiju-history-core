"""
Write your table definitions here.
"""

from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = (
    'METADATA',
    'history',
    'history_version',
    'data'
)


METADATA = sa.MetaData()  #: user tables metadata

history = sa.Table('history', METADATA,
                   sa.Column('correlation_id', sa_pg.UUID, primary_key=True),  # btree index
                   sa.Column('version', sa_pg.INTEGER, primary_key=True),
                   sa.Column('attribute', sa_pg.UUID, primary_key=True),
                   sa.Column('attribute_version', sa_pg.INTEGER, primary_key=True),
                   sa.Column('data_hash', sa_pg.UUID, default={})
                   )


attribute = sa.Table('attribute', METADATA,
                     sa.Column('id', sa_pg.UUID, primary_key=True),
                     sa.Column('attribute_name', sa_pg.TEXT),
                     sa.Column('meta', sa_pg.TEXT),
                     sa.UniqueConstraint("attribute_name", "meta", name="attr"),
                     )


history_version = sa.Table('history_version', METADATA,
                           sa.Column('correlation_id', sa_pg.UUID, primary_key=True),  # hash index
                           sa.Column('version', sa_pg.INTEGER, primary_key=True),
                           sa.Column('created', sa.DateTime, default=datetime.utcnow,
                                     server_default=sa.func.timezone(
                                         'UTC',
                                         sa.func.current_timestamp()
                                     ), nullable=False),
                           sa.Column('author', sa_pg.TEXT, nullable=True),
                           sa.Column('ttl', sa_pg.INTEGER),
                           sa.Column('tag', sa_pg.JSONB, default={},
                                     server_default=sa.text("'[]'::jsonb")),
                           sa.Column('meta', sa_pg.JSONB, default={},
                                     server_default=sa.text("'{}'::jsonb"))
                           )
sa.Index("idx_history_version", sa.nullslast(sa.desc(history_version.c.correlation_id)),
         sa.nullslast(sa.desc(history_version.c.version)))
sa.Index("idx_tag", history_version.c.tag, postgresql_using="GIN")
data = sa.Table('data', METADATA,
                sa.Column('data_hash', sa_pg.UUID, primary_key=True),
                sa.Column('data', sa_pg.JSONB, default={},
                          server_default=sa.text("'{}'::jsonb"))
                )
