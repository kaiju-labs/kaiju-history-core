FROM python:3.11-bullseye as python-builder

WORKDIR /app
ENV PATH="/app/venv/bin:$PATH"

COPY requirements.txt .

RUN python -m venv /app/venv \
  && pip install -U --no-cache-dir --no-compile pip setuptools wheel \
  && pip install --no-cache-dir --no-compile -r requirements.txt \
  && find `pwd` -name "*.pyc" -o -name "*.pyo" -o -name "__pycache__" -exec rm -rf {} \; -prune



FROM python:3.11-slim-bullseye as runner

STOPSIGNAL SIGINT
WORKDIR /app
ENTRYPOINT ["python", "-OO", "-u", "-m", "app"]

COPY --chown=999:999 app ./app
COPY --chown=999:999 settings ./settings
COPY --chown=999:999 fixtures ./fixtures
COPY --chown=999:999 --from=python-builder /app/venv ./venv

RUN groupadd -g 999 python \
  && useradd -r -u 999 -g python python -d /app \
  && chown -R python:python /app \
  && apt-get update && apt-get install -y --no-install-recommends \
  curl \
  net-tools \
  netcat \
  nano \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get purge --auto-remove \
  && apt-get clean

USER 999
ENV PATH="/app/venv/bin:$PATH"
