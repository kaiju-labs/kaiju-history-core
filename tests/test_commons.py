import pytest

__all__ = ['TestCommonMethods']


@pytest.mark.asyncio
class TestCommonMethods:
    """Test common application methods."""

    async def test_login(self, web_app, web_client, test_user):
        result = await web_client.call(
            method='auth.login', params={'username': test_user['username'], 'password': test_user['password']}
        )
        assert 'error' not in result
        session = await web_client.call(method='echo.echo_session')
        assert session.get('user_id')
        await web_client.call(method='auth.logout')
        session = await web_client.call(method='echo.echo_session')
        assert not session or not session.get('user_id')

    async def test_basic_auth(self, web_app, web_client, test_user):
        web_client._auth_str = f'Basic {test_user["username"]}:{test_user["password"]}'
        session = await web_client.call(method='echo.echo_session')
        assert session.get('user_id')

    async def test_jwt_auth(self, web_app, web_client, test_user):
        tokens = await web_client.call(
            method='auth.jwt.get', params={'username': test_user['username'], 'password': test_user['password']}
        )
        web_client._auth_str = f'Bearer {tokens["access"]}'
        session = await web_client.call(method='echo.echo_session')
        assert session.get('user_id')
        tokens = await web_client.call(method='auth.jwt.refresh', params=tokens)
        web_client._auth_str = f'Bearer {tokens["access"]}'
        session = await web_client.call(method='echo.echo_session')
        assert session.get('user_id')

    @pytest.mark.parametrize('method, params', [('rpc.tasks', None), ('Scheduler.tasks', None), ('rpc.api', None)])
    async def test_system_methods(self, web_app, web_client, method, params):
        result = await web_client.call(method, params)
        assert 'error' not in result
