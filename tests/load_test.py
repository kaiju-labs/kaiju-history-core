import random
from random_username.generate import generate_username
import aiohttp
import asyncio

max_items = 100
default_item = {
    "method": "HistoryService.import",
    "params": {
        "list_items": [
            {
                "correlation_id": "",
                "version": 0,
                "metadata": {"author": "system", "ttl": 1000},
                "attributes": [{"version": 6, "name": "", "data": ""}],
            }
        ]
    },
}


def generate_items(count_items):
    list_version = []
    list_name = generate_username(count_items)
    for name_ in list_name:
        list_version.append({"name": name_, "version": 0})
    return list_version


async def single_tread(list_items, max_items, per_second=1, count=10):
    async with aiohttp.ClientSession() as session:
        
        for i in range(count*per_second):
            rand_item = random.randint(0, max_items-1)
            col_id = list_items[rand_item]["name"]
            version = list_items[rand_item]["version"]
            list_items[rand_item]["version"] += 1
            temp = {
                "method": "HistoryService.import",
                "params": {
                    "list_items": [
                        {
                            "correlation_id": col_id,
                            "version": version,
                            "metadata": {"author": "system", "ttl": 1000},
                            "attributes": [
                                {
                                    "version": random.randint(0, 1000),
                                    "name": f"trt_{random.randint(10000, 300000)}",
                                    "data": generate_username(1)[0],
                                }
                            ],
                        }
                    ]
                },
            }
            result = await session.post("http://0.0.0.0:9099/public/rpc", json=temp)
            print(await result.json())
            print("\n")
            await asyncio.sleep(1 / per_second)


async def main():
    list_items = generate_items(max_items)
    background_tasks = []

    for i in range(100):
        task = asyncio.create_task(single_tread(list_items, max_items=max_items, per_second=100, count=120))
        background_tasks.append(task)
    await asyncio.gather(*background_tasks)

asyncio.run(main())
