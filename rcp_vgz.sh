#!/bin/bash

# Care when adapting for different repo or branch:
# 1) rsync filter rules (files and folders to include or exclude) may be unique per repo/branch.
# 2) please MAKE SURE you set correct repo/branch in BOTH ele and dgt repos
# Do not forget to update accordingly, or you will make a mess.

ELE_GITLAB="company"
DGT_GITLAB="galamart.gitlab.yandexcloud.net"
ELE_REPO="kaiju-labs/kaiju-history-core"
DGT_REPO="codebase/vsegazin/history"
ELE_BRANCH="main"
DGT_BRANCH="main"
ELE_DIR="history-ele-$ELE_BRANCH"
DGT_DIR="history-dgt-$DGT_BRANCH"

RSYNC_FILTER=$(cat << EOF
+ app/***
+ fixtures/***
+ localization/***
+ etc/***
- front/build/***
+ front/***
+ settings/***
+ settings/env.json
+ settings/config.yml
+ .dockerignore
+ requirements.txt
+ license.txt
+ Dockerfile
- *
EOF
)

DASHES="
-----"

next_stage () { sleep 2; echo "$DASHES"; }

echo "Script starting. Will attempt to check for, sync and push any new changes:"
echo
echo "FROM: $ELE_GITLAB"
echo "repo: $ELE_REPO"
echo "branch: $ELE_BRANCH"
echo
echo "TO: $DGT_GITLAB"
echo "repo: $DGT_REPO"
echo "branch: $DGT_BRANCH"

sleep 4

echo "$DASHES"
echo "Testing connectivity to gitlab servers (5s timeout, 2 attempts)"
elemento_gitlab_result=$(ssh -o ConnectTimeout=5 -o ConnectionAttempts=2 -T git@$ELE_GITLAB 2>&1)
if [[ $? -ne 0 ]]; then
	echo "Problems connecting to $ELE_REPO. Details:"
	echo "$elemento_gitlab_result"
	echo "Aborting"
	exit 1
fi
echo "$ELE_GITLAB OK"
digitech_gitlab_result=$(ssh -o ConnectTimeout=5 -o ConnectionAttempts=2 -T git@$DGT_GITLAB 2>&1)
if [[ $? -ne 0 ]]; then
	echo "Problems connecting to $DGT_REPO. Details:"
	echo "$digitech_gitlab_result"
	echo "Aborting"
	exit 1
fi
echo "$DGT_GITLAB OK"

next_stage
echo "removing potential remnants to assure clean run"
# Because i don't have time to account for all potential git-related errors with pulling into existing directories
if [[ -d $ELE_DIR ]]; then rm -rf $ELE_DIR || { echo "Problems removing folder \"$ELE_DIR\", aborting. Please remove by hand and rerun"; exit 1; } ; fi
if [[ -d $DGT_DIR ]]; then rm -rf $DGT_DIR || { echo "Problems removing folder \"$DGT_DIR\", aborting. Please remove by hand and rerun"; exit 1; } ; fi
echo "Done"

next_stage
echo "Cloning repos into subdirectories"
git clone --single-branch --branch $ELE_BRANCH git@$ELE_GITLAB:$ELE_REPO.git $ELE_DIR || { echo "Problems on cloning from $ELE_GITLAB, exiting"; exit 1; }
echo
git clone --single-branch --branch $DGT_BRANCH git@$DGT_GITLAB:$DGT_REPO.git $DGT_DIR || { echo "Problems on cloning from $DGT_GITLAB, exiting"; exit 1; }

next_stage
echo "Selectively updating digitech repo from elemento repo"
rsync -ah --stats --exclude-from=<(echo "$RSYNC_FILTER";) ./$ELE_DIR/ ./$DGT_DIR/ --delete

next_stage
echo "Preparing commit"
git -C $DGT_DIR add .
echo "Checking for changes"
git -C $DGT_DIR diff --cached --exit-code >/dev/null 2>&1 && { echo "No changes to be commited, aborting"; exit 1; }

echo "Displaying changes"
git -C $DGT_DIR status -s

next_stage
echo "Prompting for commit message"
echo
echo "HINT: last 3 commits in elemento:"
git -C $ELE_DIR log --oneline -n 3

echo "HINT: last 3 commits in digitech:"
git -C $DGT_DIR log --oneline -n 3

next_stage
echo "Please enter commit message: "
read -r commit_message
commit_result=$(git -C $DGT_DIR commit -m "$commit_message" 2>&1)

if [[ $? -ne 0 ]]; then
	next_stage
	echo "Aborting - non-zero exit code on preparing commit. Details:"
	echo
	echo "$commit_result"
	exit 1
fi

next_stage
echo "Ready to push to $DGT_BRANCH at $DGT_GITLAB:$DGT_REPO"
echo "Press y or Y to proceed / anything else to abort: "
read -r -n 1 reply
if [[ $reply == [yY]* ]]; then
	echo
	echo "Pushing commit"
	git -C $DGT_DIR push
	next_stage
	echo "Done"
else
	echo
	echo "Aborting - user choice"
fi


next_stage
echo "Cleaning up"
if [[ -d $ELE_DIR ]]; then rm -rf $ELE_DIR || { echo "Problems removing folder \"$ELE_DIR\". Please remove by hand"; } ; fi
if [[ -d $DGT_DIR ]]; then rm -rf $DGT_DIR || { echo "Problems removing folder \"$DGT_DIR\". Please remove by hand"; } ; fi

echo "Done. Exiting, bye!"
date
exit 0
